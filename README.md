# Pixel Paint

*version 0.2*

A browser-based sprite editor.

#### Clone Repo

    git clone git@gitlab.com:robertmermet/pixel-paint.git

#### View Demo

>**demo** [robertmermet.com/projects/pixel-paint](http://robertmermet.com/projects/pixel-paint)